package boonya.ben.callingwebservice

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.goddy.netelek.stores.StoresListViewModel
import com.goddy.netelek.model.Stores
import com.goddy.netelek.stores.StoresRepository
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

/**
 * Created by oozou on 7/19/2017 AD.
 */
class StoresListViewModelTest {

    @Rule @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var storesListViewModel: StoresListViewModel

    val mockedSpecies = listOf(
            Stores("Name1", "Classification1", "Lang1", "LifeSpan1"),
            Stores("Name2", "Classification2", "Lang2", "LifeSpan2"),
            Stores("Name3", "Classification3", "Lang3", "LifeSpan3"))

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        storesListViewModel = StoresListViewModel().apply {
            repository = object : StoresRepository {
                override fun getStores(successHandler: (List<Stores>?) -> Unit, failureHandler: (Throwable?) -> Unit) {
                    successHandler(mockedSpecies)
                }
            }
        }
    }

    @Test
    fun getSpecies() {
        val speciesResponse = MutableLiveData<List<Stores>>()
        val observer = mock(Observer::class.java)
        storesListViewModel.speciesResponse = speciesResponse

        speciesResponse.observeForever(observer as Observer<List<Stores>>)

        storesListViewModel.getStores()

//        verify(speciesRepo).getStores(ArgumentMatchers.any(), ArgumentMatchers.any())
        //verifyNoMoreInteractions(speciesRepo)
        verify(observer).onChanged(mockedSpecies)
        verifyNoMoreInteractions(observer)
    }


}