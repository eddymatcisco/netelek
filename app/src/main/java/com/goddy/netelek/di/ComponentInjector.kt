package com.goddy.netelek.di

import com.goddy.netelek.utils.Const

class ComponentInjector {
    companion object {
        lateinit var component: AppComponent
        fun init() {
            component = DaggerAppComponent.builder()
                    .apiModule(ApiModule(Const.BASE_URL))
                    .storeRepositoryModule(StoreRepositoryModule())
                    .build()
        }
    }
}

