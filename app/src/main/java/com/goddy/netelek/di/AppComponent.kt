package com.goddy.netelek.di

import com.goddy.netelek.stores.StoresListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
        modules = arrayOf(
                ApiModule::class,
                StoreRepositoryModule::class
        )
)
interface AppComponent {
    fun inject(storesListViewModel: StoresListViewModel)
}