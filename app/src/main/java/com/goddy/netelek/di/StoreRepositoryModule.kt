package com.goddy.netelek.di

import com.goddy.netelek.stores.StoresRepository
import com.goddy.netelek.stores.StoresRepositoryImpl
import com.goddy.netelek.api.NetelekApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StoreRepositoryModule {
    @Provides @Singleton
    fun providePostRepository(apiService: NetelekApi): StoresRepository = StoresRepositoryImpl(apiService)
}