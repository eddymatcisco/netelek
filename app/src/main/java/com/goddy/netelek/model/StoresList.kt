package com.goddy.netelek.model

import com.google.gson.annotations.SerializedName

data class StoresList(@SerializedName("storeList") val storesList: List<Stores>, val next: String?)