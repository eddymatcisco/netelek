package com.goddy.netelek.model

interface LayoutItemType {
    val layoutId: Int
}