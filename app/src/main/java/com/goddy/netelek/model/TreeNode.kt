package com.goddy.netelek.model

import java.util.ArrayList

class TreeNode<T : LayoutItemType>(var content: T?) : Cloneable {
    var parent: TreeNode<*>? = null
    private var childList: ArrayList<TreeNode<*>>? = null
    var isExpand: Boolean = false
        private set
    var isLocked: Boolean = false
        private set
    //the tree high
    private var height = UNDEFINE

    val isRoot: Boolean
        get() = parent == null

    val isLeaf: Boolean
        get() = childList == null || childList!!.isEmpty()

    init {
        this.childList = ArrayList<TreeNode<*>>()
    }

    fun getHeight(): Int {
        if (isRoot)
            height = 0
        else if (height == UNDEFINE)
            height = parent!!.getHeight() + 1
        return height
    }

    fun getChildList(): List<TreeNode<*>>? {
        return childList
    }

    fun setChildList(childList: List<TreeNode<T>>) {
        this.childList!!.clear()
        for (treeNode in childList) {
            addChild(treeNode)
        }
    }

    fun addChild(node: TreeNode<*>): TreeNode<*> {
        if (childList == null) childList = ArrayList<TreeNode<*>>()
        childList!!.add(node)
        node.parent = this
        return this
    }

    fun toggle(): Boolean {
        isExpand = !isExpand
        return isExpand
    }

    fun collapse() {
        if (isExpand) {
            isExpand = false
        }
    }

    fun collapseAll() {
        if (childList == null || childList!!.isEmpty()) {
            return
        }
        for (child in this.childList!!) {
            child.collapseAll()
        }
    }

    fun expand() {
        if (!isExpand) {
            isExpand = true
        }
    }

    fun expandAll() {
        expand()
        if (childList == null || childList!!.isEmpty()) {
            return
        }
        for (child in this.childList!!) {
            child.expandAll()
        }
    }

    fun lock(): TreeNode<T> {
        isLocked = true
        return this
    }

    fun unlock(): TreeNode<T> {
        isLocked = false
        return this
    }

    override fun toString(): String {
        return "TreeNode{" +
                "content=" + this.content +
                ", parent=" + (if (parent == null) "null" else parent!!.content!!.toString()) +
                ", childList=" + (if (childList == null) "null" else childList!!.toString()) +
                ", isExpand=" + isExpand +
                '}'.toString()
    }

    @Throws(CloneNotSupportedException::class)
    public
    override fun clone(): TreeNode<T> {
        val clone = TreeNode<T>(this.content!!)
        clone.isExpand = this.isExpand
        return clone
    }

    companion object {
        private val UNDEFINE = -1
    }
}
