package com.goddy.netelek.model

import com.goddy.netelek.R

data class Stores(
        val id : Int,
        val storeName : String
):LayoutItemType {
    override val layoutId: Int
    get() = R.layout.item_store
}
