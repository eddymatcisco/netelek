package com.goddy.netelek

import android.app.Application
import com.goddy.netelek.di.ComponentInjector


class NetelekApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        ComponentInjector.init()
    }
}