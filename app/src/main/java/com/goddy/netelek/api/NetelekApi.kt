package com.goddy.netelek.api

import com.goddy.netelek.model.StoresList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface NetelekApi {

    @Headers(
            "Authorization: Basic c3A6UmVicmVnMDk5QA==",
            "accept:application/json")
    @GET("store/")
    fun getStores(): Call<StoresList>

    @Headers(
            "Authorization: Basic c3A6UmVicmVnMDk5QA==",
            "accept:application/json")
    @GET("store/{storeId}/store")
    fun getStoresChildren(@Path("storeId") String: Any): Call<StoresList>
}
