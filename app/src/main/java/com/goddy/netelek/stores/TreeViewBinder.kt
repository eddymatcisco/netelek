package com.goddy.netelek.stores

import android.support.annotation.IdRes
import android.support.v7.widget.RecyclerView
import android.view.View
import com.goddy.netelek.model.LayoutItemType
import com.goddy.netelek.model.TreeNode

abstract class TreeViewBinder<VH : RecyclerView.ViewHolder> : LayoutItemType {
    abstract fun provideViewHolder(itemView: View): VH
    abstract fun bindView(holder: VH, position: Int, node: TreeNode<*>)
    open class ViewHolder(rootView: View) : RecyclerView.ViewHolder(rootView) {
        protected fun <T : View> findViewById(@IdRes id: Int): T {
            return itemView.findViewById(id)
        }
    }

}