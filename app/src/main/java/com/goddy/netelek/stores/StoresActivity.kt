package com.goddy.netelek.stores

import android.arch.lifecycle.*
import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View

import com.goddy.netelek.di.ComponentInjector
import com.goddy.netelek.model.Stores
import kotlinx.android.synthetic.main.activity_main.*
import com.goddy.netelek.R
import com.goddy.netelek.model.TreeNode
import java.util.*
import kotlin.collections.ArrayList

class StoresActivity : AppCompatActivity(), LifecycleRegistryOwner {

    private val registry = LifecycleRegistry(this)
    private lateinit var viewModel: StoresListViewModel
    private lateinit var rv: RecyclerView
    private lateinit var adapter: TreeViewAdapter

    override fun getLifecycle(): LifecycleRegistry = registry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        title = "NETELEK"
        viewModel = createViewModel()
        viewModel.getStores()
        rv = findViewById<RecyclerView>(R.id.rv)

        viewModel.isLoading.observe(this, Observer<Boolean> {
            it?.let {
                showLoadingDialog(it)
            }
        })
        viewModel.apiError.observe(this, Observer<Throwable> {
            //it?.let { Snackbar.make(this, "Api error", Snackbar.LENGTH_LONG).show() }
        })

        viewModel.storesResponse.observe(this, Observer<List<Stores>> {
            runOnUiThread {
                Log.e("UI_TEST","SIZE ${viewModel.nodes}")

                adapter = TreeViewAdapter(this@StoresActivity,viewModel,viewModel.nodes, Arrays.asList(StoreNodeBinder(), StoreNodeBinder()) as MutableList<TreeViewBinder<TreeViewBinder.ViewHolder>>)
                rv!!.apply {
                    setHasFixedSize(false)
                    layoutManager = LinearLayoutManager(this@StoresActivity)
                    adapter = this@StoresActivity.adapter
                }

                adapter.setOnTreeNodeListener(object : TreeViewAdapter.OnTreeNodeListener {
                    override fun onClick(node: TreeNode<Stores>, holder: RecyclerView.ViewHolder): Boolean {

                        viewModel.getChildStores(node.content!!.id)
                        viewModel.childResponse.observe(this@StoresActivity, Observer {
                            for (item: Stores in it!!) {
                                val treeNode = TreeNode(item)
                                node.addChild(treeNode)
                            }
                        })

                        if (!node.isLeaf) {
                            onToggle(!node.isExpand, holder)
                            if (!node.isExpand)
                                adapter.collapseBrotherNode(node)
                        }
                        return false
                    }

                    override fun onToggle(isExpand: Boolean, holder: RecyclerView.ViewHolder) {
                        val dirViewHolder = holder as StoreNodeBinder.ViewHolder
                        val ivArrow = dirViewHolder.ivArrow
                        val rotateDegree = if (isExpand) 90 else -90
                        ivArrow.animate().rotationBy(rotateDegree.toFloat())
                                .start()
                    }
                })
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun createViewModel(): StoresListViewModel =
            ViewModelProviders.of(this).get(StoresListViewModel::class.java).also {
                ComponentInjector.component.inject(it)
            }

    private fun showLoadingDialog(show: Boolean) {
        if (show) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
    }
}
