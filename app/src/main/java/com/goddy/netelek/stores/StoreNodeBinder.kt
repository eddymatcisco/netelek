package com.goddy.netelek.stores

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.goddy.netelek.R
import com.goddy.netelek.model.Stores
import com.goddy.netelek.model.TreeNode

class StoreNodeBinder : TreeViewBinder<StoreNodeBinder.ViewHolder>() {
    override val layoutId: Int
        get() = R.layout.item_store

    override fun provideViewHolder(itemView: View): ViewHolder {
        return ViewHolder(itemView)
    }

    override fun bindView(holder: ViewHolder, position: Int, node: TreeNode<*>) {
        holder.ivArrow.rotation = 0f
        holder.ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_right_black_18dp)
        val rotateDegree = if (node.isExpand) 90 else 0
        holder.ivArrow.rotation = rotateDegree.toFloat()
        val dirNode = node.content as Stores
        holder.tvName.text=dirNode.storeName
        if (node.isLeaf)
            holder.ivArrow.visibility = View.VISIBLE
        else
            holder.ivArrow.visibility = View.VISIBLE
    }

    class ViewHolder(rootView: View) : TreeViewBinder.ViewHolder(rootView) {
        val ivArrow: ImageView = rootView.findViewById(R.id.iv_arrow) as ImageView
        val tvName: TextView = rootView.findViewById(R.id.tv_name) as TextView

    }
}