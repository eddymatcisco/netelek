package com.goddy.netelek.stores

import com.goddy.netelek.model.Stores

interface StoresRepository {
    fun getStores(successHandler: (List<Stores>?) -> Unit, failureHandler: (Throwable?) -> Unit)
    fun getStoresById(id:Int , successHandler: (List<Stores>?) -> Unit, failureHandler: (Throwable?) -> Unit)
}