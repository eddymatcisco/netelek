package com.goddy.netelek.stores

import android.util.Log
import com.goddy.netelek.model.Stores
import com.goddy.netelek.model.StoresList
import com.goddy.netelek.api.NetelekApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StoresRepositoryImpl(val apiService: NetelekApi) : StoresRepository {
    override fun getStoresById(id: Int, successHandler: (List<Stores>?) -> Unit, failureHandler: (Throwable?) -> Unit) {
        apiService.getStoresChildren(id.toString()).enqueue(object : Callback<StoresList> {
            override fun onResponse(call: Call<StoresList>?, response: Response<StoresList>?) {
                response?.body()?.let {
                    successHandler(it.storesList)
                }
            }
            override fun onFailure(call: Call<StoresList>?, t: Throwable?) {
                failureHandler(t)
            }
        })
    }

    override fun getStores(successHandler: (List<Stores>?) -> Unit, failureHandler: (Throwable?) -> Unit) {
        apiService.getStores().enqueue(object : Callback<StoresList> {
            override fun onResponse(call: Call<StoresList>?, response: Response<StoresList>?) {
                response?.body()?.let {
                    successHandler(it.storesList)
                }
            }
            override fun onFailure(call: Call<StoresList>?, t: Throwable?) {
                failureHandler(t)
            }
        })
    }
}
