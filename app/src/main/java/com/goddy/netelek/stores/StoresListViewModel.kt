package com.goddy.netelek.stores

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.goddy.netelek.model.Stores
import com.goddy.netelek.model.TreeNode
import java.util.ArrayList
import javax.inject.Inject

class StoresListViewModel : ViewModel() {

    @Inject
    lateinit var repository: StoresRepository
    var isLoading = MutableLiveData<Boolean>()
    var apiError = MutableLiveData<Throwable>()
    val nodes = ArrayList<TreeNode<Stores>>()
    var storesResponse = MutableLiveData<List<Stores>>()
    var childResponse = MutableLiveData<List<Stores>>()
    val childNode = ArrayList<TreeNode<Stores>>()

    fun getStores() {
        isLoading.value = true
        repository.getStores(
                {
                    for (item: Stores in it!!) {
                        val store = TreeNode(item)
                        nodes.add(store)
                    }

                    Log.e("TEST RESPONSE", "RESPONSE ${it.toString()}")
                    storesResponse.value = it
                    isLoading.value = false
                    Log.e("NODE RESPONSE", "SIZE ${nodes.size}")
                },

                {
                    apiError.value = it
                    isLoading.value = false
                })
    }


    fun getChildStores(id:Int){
        Log.e("TEST RESPONSE CHILDREN", "RESPONSE $id")
        repository.getStoresById(id,{
            for (item: Stores in it!!) {
                val store = TreeNode(item)
                childNode.add(store)
            }

            Log.e("TEST RESPONSE CHILDREN", "RESPONSE ${it.toString()}")
            childResponse.value = it

            Log.e("NODE RESPONSE CHILDREN", "SIZE ${nodes.size}")

        },
        {

        })
    }

    /**
     * Adapter Callback
     */

    fun getStoresAt(position: Int): Stores? {
        return if (position < getStoresSize()) {
            storesResponse.value?.get(position)
        } else {
            null
        }
    }

    fun getStoresSize(): Int {
        storesResponse.value?.let {
            return it.size
        }
        return 0
    }

}